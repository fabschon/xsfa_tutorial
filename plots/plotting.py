# import everything we need
import matplotlib.pyplot as plt
import mdp
import numpy as np
from scipy.io import wavfile
plt.style.use('seaborn-pastel')


def timeplot(sig1, sig2, fs, label1, label2):
    # get time increment and generate time axis
    dt = 1/fs
    time = np.linspace(0, dt*sig1.shape[0], sig1.shape[0])

    # get figure and subplots
    fig = plt.figure()
    axsig1 = fig.add_subplot(211)
    axsig2 = fig.add_subplot(212)

    # plot
    axsig1.plot(time, sig1, color='k', linewidth=0.05)
    axsig2.plot(time, sig2, color='k', linewidth=0.05)

    # set axes limits
    axsig1.set_xlim([time[0], time[-1]])
    axsig2.set_xlim([time[0], time[-1]])

    # label and show
    axsig1.set_ylabel(label1)
    axsig2.set_ylabel(label2)
    axsig2.set_xlabel('time')
    plt.show()


def getslowness(sig):
    deri = [sig[i+1]-sig[i] for i in range(sig.shape[0]-1)]
    return np.mean(np.square(deri))
if __name__=='__main__':

    # get sound data
    fs1, s1 = wavfile.read('../airraidsiren.wav')
    fs2, s2 = wavfile.read('../housemusic.wav')

    # make sure source sequences have the same length, the correct data type and are bounded by 1,-1
    s1 = s1[: np.min((s1.shape[0], s2.shape[0]))].astype('float64')
    # scaled in order to produce the twirling mixture seen further below
    s1 = s1/np.max(np.abs(s1))
    s2 = s2[: np.min((s1.shape[0], s2.shape[0])), 0].astype('float64')
    s2 = s2/np.max(np.abs(s2))  # scaled as above

    # print source information

    print( 'Shape of sequence 1: ', s1.shape, '// Shape of sequence 2: ', s2.shape )
    print( 'Frequency of sequence 1 (Hz): ', fs1, '// Frequency of sequence 2 (Hz): ', fs2 )
    print(getslowness(s1), getslowness(s2))
    print( np.power(getslowness(s2), 3./2.)/np.square(getslowness(s2)-getslowness(s1)) )

    # visualize downsampled source sequence
    plt.plot(s1[::10], s2[::10], 'kx', markersize=.1)
    plt.xlabel('source 1')
    plt.ylabel('source 2')
    plt.title('source sequence')
    plt.show()

    timeplot(s1[::10], s2[::10], fs1, 'source 1', 'source 2')
    # do the nonlinear transformation
    x1 = np.multiply(s2 + (3*s1) + 6, np.cos(1.5*np.pi*s1))
    x2 = np.multiply(s2 + (3*s1) + 6, np.sin(1.5*np.pi*s1))

    # visualize downsampled mixture sequence
    plt.plot(x1[::10], x2[::10], 'kx', markersize=.1)
    plt.axis([-9, 9, -9, 9])
    plt.xlabel('mixture component 1')
    plt.ylabel('mixture component 2')
    plt.title('mixture sequence')
    plt.show()


    timeplot(x1[::10], x2[::10], fs1,
             'mixture component 1', 'mixture component 2')
    o_mix = 15   # expansion order of the mixture
    o_est = 13   # expansion order of the estimated sources
    n_s = 2    # number of sources to estimate

    # initialise expansion nodes
    basic_exp = (mdp.nodes.PolynomialExpansionNode, (o_mix, ), {})
    intern_exp = (mdp.nodes.PolynomialExpansionNode, (o_est, ), {})

    # initialize xSFA
    xsfa = mdp.nodes.XSFANode(
        basic_exp=basic_exp, intern_exp=intern_exp, output_dim=n_s)

    # compute the slow features
    s_est = xsfa.execute(np.transpose(np.array([x1, x2])))

    # visualize downsampled source estimation
    plt.plot(s_est[:, 0][::4], s_est[:, 1][::4], 'kx', markersize=.1)
    plt.axis([-2.5, 2.5, -2.5, 2.5])
    plt.xlabel('estimated source 1')
    plt.ylabel('estimated source 2')
    plt.title('sources estimated by xSFA')
    plt.show()

    timeplot(s_est[:, 0][::4], s_est[:, 1][::4], fs1,
             'estimated source 1', 'estimated source 2')
    # write sounds to file
    wavfile.write( 'sourceestimation1.wav', fs1, s_est[:,0] )
    wavfile.write( 'sourceestimation2.wav', fs2, s_est[:,1] )
    wavfile.write( 'mixture1.wav', fs1, x1 )
    wavfile.write( 'mixture2.wav', fs2, x2 )

    fig = plt.figure()
    axc1 = fig.add_subplot(121)
    axc2 = fig.add_subplot(122)

    axc1.plot( s1, s_est[:,0], 'kx', markersize=.1 )
    axc2.plot( s2, s_est[:,1], 'kx', markersize=.1 )
    axc1.set_xlabel( 'source 1' )
    axc1.set_ylabel( 'estimated source 1' )
    axc2.set_xlabel( 'source 2' )
    axc2.set_ylabel( 'estimated source 2' )
    plt.suptitle( 'Comparing source estimations to original sources' )
    plt.show()
